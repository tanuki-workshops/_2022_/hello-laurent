const fs = require("fs")

let start = new Date()

// ==================================
//  Severity:
//  Critical, High, Medium
// ==================================
let severity = "Critical"

// ==================================
//  Confidence:
//  Unknown Confirmed Experimental
// ==================================
let confidence = "Confirmed"

// ==================================
//  Scanner's info
// ==================================
let scannerName = "GitLab"
let scannerId = "gitlab"
let scannerSrc = "https://gitlab.com"
let scannerVendor = "GitLab"
let scannerVersion = "1.2.7"

// ==================================
//  Vulnerabilities' list
// ==================================
let vulnerabilities = [
  {
    category: "sast",
    name: `xyz`,
    message: `🔴 vulnerability xyz detected`,
    description: `🖐️ vulnerability xyz was detected`,
    cve: "000001",
    severity: "Critical", // Critical, High, Medium
    confidence: "Confirmed", // Unknown Confirmed Experimental
    scanner: {id: scannerId, name: scannerName},
    location: {
      file: "hello/main.go", 
      start_line: 1,
      end_line: 1,
      class: "owasp",
      method: "generic",
      dependency: {package: {}}
    },
    identifiers: [
      {
        type: "xyz_rule_id",
        name: "xyz rule cwe",
        value: "owasp xyz"
      }
    ]
  },
  {
    category: "sast",
    name: `abc`,
    message: `🟠 vulnerability abc detected`,
    description: `🖐️ vulnerability abc was detected`,
    cve: "000002",
    severity: "Critical", // Critical, High, Medium
    confidence: "Confirmed", // Unknown Confirmed Experimental
    scanner: {id: scannerId, name: scannerName},
    location: {
      file: "morgen/main.go", 
      start_line: 1,
      end_line: 1,
      class: "owasp",
      method: "generic",
      dependency: {package: {}}
    },
    identifiers: [
      {
        type: "abc_rule_id",
        name: "abc rule cwe",
        value: "owasp abc"
      }
    ]
  }                    
              
]

let end = new Date() - start

// ==================================
//  SAST Report
// ==================================
let sastReport = {
  version: "3.0",
  vulnerabilities: vulnerabilities,
  remediations: [],
  scan: {
    scanner: {
      id: scannerId,
      name: scannerName,
      url: scannerSrc,
      vendor: {
        name: scannerVendor
      },
      version: scannerVersion
    },
    type: "sast",
    start_time: start,
    end_time: end,
    status: "success"
  }
}

fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))
